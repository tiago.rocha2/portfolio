<?php 
get_header();

/*Template Name: Work */
?>

<main class="work">
        <h1 class="title">Work</h1>
        <div class="features">

            <div class="feature">
                <img src="wp-content/themes/theme/assets/Rectangle-30.svg" class="DB" />
                <div class="content">
                    <h2><a href="http://localhost:5000/?page_id=17" class="design"> Designing Dashboards</a></h2>
                    <a class="data">2020</a> <a> Dashboard</a>
                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                </div>
            </div>
            <hr class="solid">
            <div class="feature">
                <img src="wp-content/themes/theme/assets/Rectangle 32.svg" alt="WM" />
                <div class="content">
                    <h2>Vibrant Portraits of 2020</h2>
                    <a class="data">2018</a> <a> Illustration</a>
                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                </div>
            </div>
            <hr class="solid">
            <div class="feature">
                <img src="wp-content/themes/theme/assets/Rectangle-34.svg" alt="A" />
                <div class="content">
                    <h2>36 Days of Malayalam type</h2>
                    <a class="data">2018</a> <a> Typography</a>
                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                </div>
            </div>
            <hr class="solid">
            <div class="feature">
                <img src="wp-content/themes/theme/assets/Rectangle 40.svg" alt="A" />
                <div class="content">
                    <h2>Components</h2>
                    <a class="data">2018</a> <a> Components, Design</a>
                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                </div>
            </div>
            <hr class="solid">
        </div>




<?php get_footer();?>
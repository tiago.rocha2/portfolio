<?php

function load_stylesheets()
{
    wp_register_style('stylesheet', get_template_directory_uri() . '/dev/prod/scripts.css', array(), 1, 'all');
    wp_enqueue_style('stylesheet');


    wp_register_style('custom', get_template_directory_uri() . '/dev/prod/scripts.js', array(), 1, 'all');
    wp_enqueue_style('custom');

}

add_action('wp_enqueue_scripts', 'load_stylesheets');


//menu support
add_theme_support('menus');



register_nav_menus(

    array(

        'top-menu' => __('Top menu', 'theme'),

    )
);
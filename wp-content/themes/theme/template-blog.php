<?php 
get_header();

/*Template Name: Blog */
?>

<main class="blog">
        <h1 class="nome">Blog</h1>
        <div class="features">
            <div class="feature">
                <div>
                    <h2>UI Interactions of the week</h2>
                    <a>12 Feb 2019 | Express, Handlebars</a>
                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                </div>
                <hr class="solid">
                <div>
                    <h2>UI Interactions of the week</h2>
                    <a>12 Feb 2019 | Express, Handlebars</a>
                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                </div>
                <hr class="solid">
                <div>
                    <h2>UI Interactions of the week</h2>
                    <a>12 Feb 2019 | Express, Handlebars</a>
                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                </div>
                <hr class="solid">
                <div>
                    <h2>UI Interactions of the week</h2>
                    <a>12 Feb 2019 | Express, Handlebars</a>
                    <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                </div>
                <hr class="solid">
            </div>
        </div>
    </main>

<?php get_footer();?>
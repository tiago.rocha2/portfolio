<?php 
get_header();

/*Template Name: Contact*/
?>
<main class="contact">
        <div class="h1">
            <h1>This Page will show You, <br> Where to Find Us!!</h1>
        </div>
        <div class="features">
            <div class="present">
                <div>
                    <h2>Hi, I am Tiago,</br>You can find me at:</h2>
                    <p>WhatsApp: +55 (31) 99601-7419</p>
                    <p>Gmail: tiagobtrc@gmail.com</p>
                    <p>GitHub: github.com/tiagorochaB</p>

                </div>
                <div>
                    <img class="foto" src="wp-content/themes/theme/assets/eumemo-300x300.png" />
                </div>

            </div>

            <div>
                <hr class="solid">
            </div>


            <div class="present">
                <div>
                    <h2>Hi, I am Pedro,</br>You can find me at:</h2>
                    <p>WhatsApp: +55 (37) 99837-7347</p>
                    <p>Gmail: pedro.galvao@brius.com.br</p>
                    <p>GitHub: github.com/galvaoevaristo</p>
                </div>

                <div>
                    <img class="foto" src="wp-content/themes/theme/assets/Pedro.png" />
                </div>

            </div>

            <div>
                <hr class="solid">
            </div>
        </div>
                



<?php get_footer();?>
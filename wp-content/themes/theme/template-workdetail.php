<?php 
get_header();

/*Template Name: Work-detail */
?>

<main class="detail">
        <div class="first">
            <h1 class="nome">Designing Dashboards with usability in mind</h1>
            <a class="date">2020</a>
            <a> Dashboard, User Experience Design</a>
            <p class="desc">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet</br>
                sint. Velit officia consequat duis enim velit mollit. Exercitation veniam</br>
                consequat sunt nostrud amet.</p>
            <img src="wp-content/themes/theme/assets/Rectangle 4.svg" alt="DB" />
        </div>
        <div>
            <div class="first">
                <h1 class="nome">Heading 1</h1>
                <h2>Heading 2</h2>
                <p class="desc">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet</br>
                    sint. Velit officia consequat duis enim velit mollit. Exercitation veniam</br>
                    consequat sunt nostrud amet.</p>
                <img src="wp-content/themes/theme/assets/Rectangle 5.svg" alt="" />
                <img src="wp-content/themes/theme/assets/Rectangle 6.svg" alt="" />
            </div>
        </div>

    </main>

<?php get_footer();?>
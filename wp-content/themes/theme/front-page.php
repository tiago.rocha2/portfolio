<?php get_header();?>

<main>
        <div class="bio">

            <div>
                <h1 class="nome">Hi, I am Tiago,</br>Creative Technologist</h1>
                <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet</br>
                    sint. Velit officia consequat duis enim velit mollit. Exercitation veniam</br>
                    consequat sunt nostrud amet.</p><br>
                <button class="button">Download Resume</button>
            </div>
            <div>
                <img class="foto" src="wp-content/themes/theme/assets/eumemo-300x300.png" />
            </div>
        </div>

        <!-- Esse é o conteúdo principal da nossa página -->


        <!-- Contém um artigo -->
        <div class="bio-mid">
            <section class="bg-blue">
                <div class="recent">
                    <div class="title">
                        <h3>Recent Posts</h3>
                        <ul>
                            <a class="all">View All</a>
                        </ul>
                    </div>
                    <div class="pub">
                        <div class="post">
                            <h2>Making a design system from scratch</h2>
                            <a>12 Feb 2020 | Design, Pattern</a>
                            <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet. </p>
                        </div>
                        <div class="post">
                            <h2>Creating pixel perfect icons in Figma</h2>
                            <a>12 Feb 2020 | Figma, Icon Design</a>
                            <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <div class="bio-last">

            <div class="features">
                <div>
                    <h3>Featured Works</h3>
                </div>
                <div class="feature">
                    <img src="wp-content/themes/theme/assets/Rectangle-30.svg" class="DB" />
                    <div class="content">
                        <h2>Designing Dashboards</h2>
                        <a class="data">2020</a>
                        <a> Dashboard</a>
                        <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                    </div>
                </div>
                <hr class="solid">
                <div class="feature">
                    <img src="wp-content/themes/theme/assets/Rectangle 32.svg" alt="WM" />
                    <div class="content">
                        <h2>Vibrant Portraits of 2020</h2>
                        <a class="data">2018</a>
                        <a> Illustration</a>
                        <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>
                    </div>
                </div>
                <hr class="solid">
                <div class="feature">
                    <img src="wp-content/themes/theme/assets/Rectangle-34.svg" alt="A" />
                    <div class="content">
                        <h2>36 Days of Malayalam type</h2>
                        <a class="data">2018</a>
                        <a> Typography</a>
                        <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>

                    </div>
                </div>
                <hr class="solid">
            </div>
        </div>

<?php get_footer();?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PortFolio Brio</title>
    <link href="https://fonts.googleapis.com/css?family=Heebo:400,500,700,800|Fira+Sans:600" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <?php wp_head();?>
  </head>

  <body class="main">
  <header>
        <div class="navbar" id="myTopnav">
          <a href="http://localhost:5000/" class="active"> Home</a>
          <a href="http://localhost:5000/?page_id=11" > Blog</a>
          <a href="http://localhost:5000/?page_id=13" > Works</a>
          <a href="http://localhost:5000/?page_id=15" > Contact</a>          
          <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <script>
            function myFunction() {
                var x = document.getElementById("myTopnav");
                if (x.className === "navbar") {
                    x.className += " responsive";
                } else {
                    x.className = "navbar";
                }
            }
        </script>
        
    </header>
    